package main

import (
	"gitlab.com/jackorp/remote_builder/build/server"
	"log"
	"os"
)

func arg_err() {
	prog_name := os.Args[0]

	log.Fatal("Expected `" +
		prog_name +
		" <listen_ip> <listen_port>'")
}

func main() {

	run_args := os.Args[1:]

	if len(run_args) <= 1 {
		arg_err()
	}

	if len(run_args) == 2 {
		server.Server(run_args[0], run_args[1])
	} else {
		arg_err()
	}
}
