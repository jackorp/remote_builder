package main

import (
	"gitlab.com/jackorp/remote_builder/build/client"
	"flag"
	"log"
	"os"
)

func arg_err() {
	prog_name := os.Args[0]

	log.Fatal("Expected `" +
		prog_name +
		"[options] <package_path> <remote_ip> <remote_port>'`")
}

func main() {
	var mockroot string
	flag.StringVar(&mockroot, "mockroot", "fedora-rawhide-x86_64", "Specify a valid mockroot that the server should use.")

	flag.Parse()
	run_args := flag.Args()

	if len(run_args) >= 3 {
		client.Client(run_args[0], run_args[1], run_args[2], mockroot)
	} else {
		arg_err()
	}
}
