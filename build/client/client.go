package client

import (
	"gitlab.com/jackorp/remote_builder/build/communication/directives"
	"gitlab.com/jackorp/remote_builder/build/communication/actions"
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

func fileSize(file *os.File) int64 {
	fstat, err := file.Stat()
	if err != nil {
		log.Fatal(err)
	}

	return fstat.Size()
}

func printRemoteLog(connection io.Reader) {
	for {
		message, err := bufio.NewReader(connection).ReadString('\n')
		if err != nil {
			if err != io.EOF {
				log.Fatal(err)
			}
			break
		}
		fmt.Print("REMOTE->", message)
	}
}

func Client(srpm_path, server, port, mockroot string) {
	c, err := net.Dial("tcp", server + ":" + port)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	err = actions.RequestMockroot(c, mockroot)
	if err != nil {
		log.Fatal(err)
	}

	err = directives.ReceivedDirective(c)
	if err != nil {
		log.Fatal(err)
	}

	file, err := os.Open(srpm_path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	file_size := fileSize(file)

	err = directives.SendFileDirective(c, srpm_path, file_size)
	if err != nil {
		log.Fatal(err)
	}

	err = directives.ReceivedDirective(c)
	if err != nil {
		log.Fatal(err)
	}

	err = directives.SendFile(c, file, file_size)
	if err != nil {
		log.Fatal(err)
	}

	printRemoteLog(c)

	fmt.Println("Server ended conversation. My job is done.")
}
