package directives

import (
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"strconv"
)

type ClientError struct {
	error
	// String for now, but Enum would be better in the future.
	reason string
}

func (e *ClientError) Error() string {
	return fmt.Sprintf("Error reading: %v", e.reason)
}

func (e *ClientError) assingReason(reason string) {
	e.reason = reason
}

func CreateClientError(reason string) *ClientError {
	return &ClientError{reason: reason}
}

func SendFileDirective(connection io.Writer, srpm_path string, file_size int64) error {
	file_command := []byte("FILE " + path.Base(srpm_path) + " SIZE " + strconv.FormatInt(file_size, 10) + "\n")
	log.Print("Write command")
	written, err := connection.Write(file_command)
	if err != nil {
		return err
	}

	if written != len(file_command) {
		return CreateClientError("Failed to send full file directive.")
	}

	log.Print("Wrote command")
	return nil
}

func ReceivedDirective(connection io.Reader) error {
	client_err := CreateClientError("")
	// We expect a very small message returned.
	response_buf_size := 128
	response_buffer := make([]byte, response_buf_size)
	read, err := connection.Read(response_buffer)
	if err != nil {
		client_err.assingReason(err.Error())
		return client_err
	}
	if read >= 128 {
		client_err.assingReason("Response too long")
		return client_err
	}

	if read == 0 {
		client_err.assingReason("0 length Response received")
		return client_err
	}

	server_response := string(response_buffer[:read])

	if server_response != "ACCEPT_OK\n" {
		client_err.assingReason("Server did not correctly acknowledge the transfer.")
		return client_err
	}

	return nil
}

func SendFile(connection io.Writer, file *os.File, write_file_size int64) error {
	client_err := CreateClientError("")
	log.Print("Writing file")
	written, err := io.Copy(connection, file)
	if err != nil {
		client_err.assingReason(err.Error())
		return client_err
	}


	if written != write_file_size {
		reason := fmt.Sprintf("File write was only partial. Size is: %d but sent only %d", write_file_size, written)
		client_err.assingReason(reason)
		return client_err
	}

	return nil
}
