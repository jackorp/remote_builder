package actions

import (
	"fmt"
	"io"
	"log"
)


type clientError struct {
	error
	// String for now, but Enum would be better in the future.
	reason string
}

func (e *clientError) Error() string {
	return fmt.Sprintf("Error reading: %v", e.reason)
}

func (e *clientError) assingReason(reason string) {
	e.reason = reason
}

func createClientError(reason string) *clientError {
	return &clientError{reason: reason}
}


func RequestMockroot(connection io.Writer, mockroot string) (error) {
	log.Print("Requesting mockroot ", mockroot)

	mockroot_request := []byte("MOCKROOT " + mockroot + "\n")

	written, err := connection.Write(mockroot_request)
	if err != nil {
		return err
	}

	if written != len(mockroot_request) {
		return createClientError("Could not request mockroot.")
	}

	return nil
}
