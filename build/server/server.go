package server

import (
	"bufio"
	"log"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func Server(ip string, port string) {
	listener, err := net.Listen("tcp", ip + ":" + port)
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	log.Println("Started server listening on", ip, "on port", port)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println("Error accepting client:", err)
			continue
		}

		go handleClient(conn)
	}
}

func handleClient(conn net.Conn) {
	defer conn.Close()
	buffer := make([]byte, 1024)
	
	log.Println("Accepted connection")

	reader := bufio.NewReader(conn)

	read, err := reader.Read(buffer)
	if err != nil {
		log.Println("Failed to read from client.\n", err)
		return
	}

	log.Println("Read", read, "bytes.")

	newline_idx := strings.IndexByte(string(buffer), '\n')
	if newline_idx == -1 {
		log.Println("Did not find a newline. Possibly malformed request from client.\n", "Buffer:", buffer)
		return
	}

	log.Println("Started parsing.")

	mockroot_directive_str := "MOCKROOT "
	if read < len(mockroot_directive_str) {
		log.Println("Malformed request, expected MOCKROOT directive.")
	}
	mockroot_start := strings.Index(string(buffer[:read]), " ") + 1
	mockroot := string(buffer[mockroot_start:newline_idx])

	response := []byte("ACCEPT_OK\n")

	log.Println("Accepted mockroot", mockroot)
	conn.Write(response)

	read, err = reader.Read(buffer)
	if err != nil {
		log.Println("Failed to read from client.\n", err)
		return
	}
	newline_idx = strings.IndexByte(string(buffer), '\n')
	if newline_idx == -1 {
		log.Println("Did not find a newline. Possibly malformed request from client.\n", "Buffer:", buffer)
		return
	}

	file_directive_str := "FILE "
	command := string(buffer[:newline_idx])
	directive_end := strings.Index(command, file_directive_str)
	if directive_end == -1 {
		log.Println("Malformed request. Expected FILE directive, found:", command)
		return
	}

	file_info := command[directive_end:]

	log.Println("Got file info directive:", file_info)

	size_directive_str := " SIZE "
	size_directive := strings.Index(file_info, size_directive_str)

	log.Println("Got SIZE directive")

	if size_directive == -1 {
		log.Println("Malformed request. Expected SIZE directive, found:", command)
		return
	}

	size, err := strconv.ParseInt(file_info[size_directive+len(size_directive_str):], 10, 64)
	if err != nil {
		log.Println("Cannot parse file size. Buffer:", file_info[size_directive:])
	}

	log.Println("Expecting file of size", size)

	filename := command[len(file_directive_str):size_directive]
	if len(filename) <= 0 {
		log.Println("No filename specified. Invalid request.")
		return
	}

	if strings.Index(filename, "/") != -1 {
		log.Println("Invalid filename.")
		return
	}

	response = []byte("ACCEPT_OK\n")

	conn.Write(response)

	output_file, err := os.Create(filename)
	if err != nil {
		log.Println("Could not open file for reading.\n", err)
	}
	defer output_file.Close()

	log.Println("Created new file", filename)

	for size > 0 {
		read, err := reader.Read(buffer)
		if err != nil {
			log.Println("Could not read from client.\n", err)
			return
		}

		written, err := output_file.Write(buffer[:read])
		if err != nil {
			log.Println("Could not write to file '", filename, "'\n", err)
			return
		}

		size -= int64(written)
	}

	cmd := exec.Command("mock", "--verbose", "-r", mockroot, "--resultdir", "result", "--no-cleanup-after", filename)

	writer := bufio.NewWriter(conn);

	cmd.Stderr = writer
	cmd.Stdout = writer

	err = cmd.Start()
	log.Println("Starting mock build.")
	if err != nil {
    log.Println("Failed to start mock.", err)
		return
	}

	err = cmd.Wait()
	if err != nil {
		log.Println("Something went wrong with mock.", err)
		return
	}

	log.Println("Finished. Ending conversation.")
}
