# Remote builder project

## Motivation

When updating RPM packages, I like to rebuild them locally first to ensure that they at least finish
build. However, in case of bigger compiled projects, this can take even 40 minutes with some parts
taking 100% of my CPU. So I started doing the builds on remote hosts, like servers and more powerful
desktops. The speed-up is up to 5 times less time spent on the entire build from start to finish,
including tests.

This has its shares of problems though. To build in mock on a remote host we have to:
1. Have an SSH session
1. Move the package onto the host, I usually via SCP. While some NFS or SSHFS would help, setting
   that up and maintaing that share point is its own problem.

This introduces slowdowns in flow. I have to type password for SSH keys, sometimes multiple times.

Therefore this project.

## Usage

### Build

There are 2 binaries. Client's `remote_builder_client` and Server's `remote_builder_server`.

From project's root execute:

* To build client
  ```console
  $ go build ./bin/remote_builder_client/
  ```
* To build server
  ```console
  $ go build ./bin/remote_builder_server/
  ```

This will create the binaries with the name of the directory, so after executing those 2 steps, you
will see `remote_builder_client` and `remote_builder_server` in the current directory.

### Using client

Using client is trivial. You just specify the SRPM you want to build, IP and port of the remote.
For example:
```console
client $ remote_builder_client test-1.2-1.src.rpm <Remote IP> <Remote Port>
```
You can of course also use 127.0.0.1. I arbitrarily use port 4556 for this program.

This command will launch a build for package `test-1.2-1.src.rpm`. By default,
`fedora-rawhide-x86_64` is used. Read on to learn how to request a different mockroot for a package
build.

#### Specifying mockroot

You can request a specific mockroot from server. Server will use that mockroot as long as it exists.
Otherwise running the mock command fails.

To request for example a `centos-stream-9-x86_64` mockroot:
```console
client $ remote_builder_client --mockroot centos-stream-9-x86_64 test-1.2-1.src.rpm <Remote IP> <Remote Port>
```

!!! Thanks to the terribleness and inflexibility of the `flags` golang package, `--mockroot`
(or `-mockroot`) is sensitive to position. If it is not specified first, you will not specify a
mockroot properly (and probably fail the request for build somewhere down the road).

### Running Server

Server accepts 2 positional arguments.

1. First is the IP listen address. For localhost only, 127.0.0.1 is
   enough. 0.0.0.0 to listen for any incoming connection.
2. Second is the port the server is listening on. I usually use 4556, because why not.
   You will most probably need some firewall rule to enable incoming connections to reach the
   program

!!! Caveat: there is 0 SSL so far and there might be bugs. Pretty please, do not use it on publicly
reachable servers. (Unless *you* control the entire network at least up to the closes public
network facing router/firewall, do not allow firewall to accept the connection)

Generally, I use the following form to start the server:
```console
$ remote_builder_server <IP> <PORT>
```
To listen for any connections on port 4556:
```console
server $ remote_builder_server 0.0.0.0 4556
```

#### I control the firewall and the network. And I have firewall-cmd.

Allow your port in the firewall.
```console
server $ sudo firewall-cmd --add-port=4556/tcp --permanent
server $ sudo firewall-cmd --reload
```
I added `--permanent` in anticipation that you will run on the server more than once.
If you don't always run it, feel free to remove the `--permanent` and run it on demand. Also do not
restart the firewall if your rule is not permanent

Start the server listening for any incoming connections:
```console
server $ remote_builder_server 0.0.0.0 4556
```

Then you can use the remote IP of the server for client:
```
client $ remote_builder_client <SRPM> 192.168.127.45 4556
```

#### Using SSH port forwarding

Instead of allowing connections to the port via firewall, we can utilize port forwarding in SSH.
```console
client $ p=4556; ssh -N -L $p:localhost:$p remoteuser@remotehost
```
```console
server $ remote_builder_server 127.0.0.1 4556
```
Thanks to the SSH port forwarding, we just need the server to listen on a local interface.

Then in client, where the SSH is forwarding the port to, you just simply use IP 127.0.0.1 and port
4556.

## Layout

This is a regular TCP client-server application with a custom communication "protocol".

### Communication

Client requests a mockroot and sends the SRC.RPM file. Server on success acks each one of these
actions with `ACCEPT_OK\n`. If an error occurs, no handling is currently in place to notify the
client (outside of closing connection).

![Communication sequence diagram](./docs/figure/client_server.png)

## Development Philosophy

The entire client/server program is done with only the golang's standard library. No external
dependencies outside of what golang provides. One possible future exception is the _ini_
configuration file parsing library.
